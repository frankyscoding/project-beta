import React, {useEffect, useState} from 'react';

function TechniciansForm(props) {
    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.name = name;
        data.employee_number = employeeNumber;

        console.log(data);

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);

            setName('');
            setEmployeeNumber('');
        }

    }







return (
  <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://media.tenor.com/pzw3h9-iRkEAAAAM/mekanik.gif" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-technician-form">
                <h1 className="card-title">New Technician Form</h1>
                <p className="mb-3">
                  Please enter new technician's details into the system.
                </p>




                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} value={name} required placeholder="closet name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">Technician's name </label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmployeeNumberChange} value={employeeNumber} required placeholder="employee number" type="employee_number" id="employee_number" name="employee_number" className="form-control" />
                      <label htmlFor="employee_number">Employee number</label>
                    </div>
                  </div>


                </div>
                <button className="btn btn-lg btn-primary">Create technician!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! Your new guy has been saved.
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
);

}

export default TechniciansForm;
