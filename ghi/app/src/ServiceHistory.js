import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";



function ServiceHistory() {

    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');

    const handleSearch = async (event) => {
        const value = event.target.value
        setSearch(value)
    }

    async function fetchAppointments() {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        const data = await response.json();
        const appointmentData = data.appointments
        const result = appointmentData.filter(appointment => appointment.vin === search)
        setAppointments(result);

      }

      useEffect(() => {
        fetchAppointments();
      }, []);


    //////////////// ////////////VIP STATUS DETECTION//////////////// ////////////////

    const [automobiles, setAutomobiles] = useState([])


    async function fetchAutomobiles() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        const data = await response.json();
        const automobileData = data.autos
        setAutomobiles(automobileData);
    }

    useEffect(() => {
        fetchAutomobiles();
      }, []);
      console.log(automobiles)

    function vipStatus(appointment, automobiles) {
        let output = []
        for (let object of automobiles) {
          output.push(object["vin"])
        }
        automobiles = output

        if (automobiles.includes(appointment["vin"])) {
            return <img src='https://www.pngfind.com/pngs/m/0-1183_vector-flash-vip-diamond-png-free-photo-clipart.png' width="40" />
        }

        }


    //////////////// //////////////// //////////////// //////////////// ////////////////
        function isFinished(appointment) {
            if (appointment["completed"] === true) {
            return "✅"
            }
        }
    //////////////// //////////////// //////////////// //////////////// ////////////////


    return (
        <>
        <center>
        <h1>Service Appointment History</h1>
        <h2>Search by VIN</h2>
        </center>

        <div className="input-group">
            <input onChange={handleSearch} value={search} type="search" id="form1" className="form-control" />

            <button type="button" onClick={fetchAppointments} className="btn btn-primary">
                Search
            </button>


        </div>



            <table className="table table-striped table-hover align-middle mt-5">

            <thead>
                <tr>
                <th>VIN</th>
                <th>VIP status</th>
                <th>Customer name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Completed?</th>



                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ vipStatus(appointment, automobiles) }</td>
                            <td>{ appointment.customer_name }</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString() }</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString("en-US", {hour: '2-digit', minute:'2-digit'}) }</td>
                            <td>{ appointment.technician }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ isFinished(appointment) }</td>

                        </tr>
                    );
                })}
            </tbody>
            </table>
        </>

    )
}


export default ServiceHistory;
