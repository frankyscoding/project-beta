
import React from 'react'

function AutomobileList({ automobiles }) {
    if (automobiles === undefined) {
        return null
    }
  return (
    <>

    <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>


            </tr>
          </thead>
          <tbody>
            {automobiles.map(automobile => {
                return (
                    <tr key={automobile.href}>
                        <td>{ automobile.vin }</td>
                        <td>{ automobile.color }</td>
                        <td>{ automobile.year }</td>
                        <td>{ automobile.model.name }</td>
                        <td>{ automobile.model.manufacturer.name }</td>
                    </tr>
                );
            })}
          </tbody>
        </table>
        </>
  )
}

export default AutomobileList
