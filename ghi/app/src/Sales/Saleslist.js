import React from 'react'

function Saleslist({ sales }) {
  if (sales === undefined) {
    return null
}
  return (
    <>
    <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Employee number</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale price</th>


            </tr>
          </thead>
          <tbody>
            {sales.map(sale => {
                return (
                    <tr key={sale.id}>
                        <td>{ sale.sales_person.employee_name }</td>
                        <td>{ sale.sales_person.employee_number }</td>
                        <td>{ sale.customer.customer_name }</td>
                        <td>{ sale.automobile.vin }</td>
                        <td>${ sale.price }</td>
                    </tr>
                );
            })}
          </tbody>
        </table>

        </>
  )
}

export default Saleslist
