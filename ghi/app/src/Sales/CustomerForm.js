import React, {useState, useEffect} from 'react'

function CustomerForm() {
    const [name, setName] = useState('')
    const Namechange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [address, setAddress] = useState('')
    const addresschange = (event) => {
        const value = event.target.value;
        setAddress(value)
    }

    const [phonenum, setPhonenum] = useState('')
    const phonenumchange = (event) => {
        const value = event.target.value;
        setPhonenum(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.customer_name = name
        data.address = address
        data.phone_number = phonenum


        const customersURL = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(customersURL, fetchConfig)
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);

            setName('');
            setAddress('');
            setPhonenum('');
        }
    }

  return (
    <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-customer-form">
                      <h1 className="card-title">Create a customer</h1>

                      <div className="row">
                        <div className="col">
                          <div className="form-floating mb-3">
                            <input onChange={Namechange}  required placeholder="Name" type="text" id="customer_name" name="customer_name" className="form-control" value={name}/>
                            <label htmlFor="customer_name">Name</label>
                          </div>
                          <div className="form-floating mb-3">
                            <input onChange={addresschange}  required placeholder="Address" type="text" id="address" name="address" className="form-control" value={address}/>
                            <label htmlFor="address">Address</label>
                          </div>
                          <div className="form-floating mb-3">
                            <input onChange={phonenumchange}  data-mdb-input-mask="+48 999-999-999" required placeholder="Phone number" type="text" id="phone_number" name="phone_number" className="form-control" value={phonenum}/>
                            <label htmlFor="phone_number">Phone number</label>
                          </div>
                        </div>


                      </div>
                      <button className="btn btn-lg btn-primary">Create</button>
                    </form>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                      Congratulations! Customer has been saved.
                    </div>
                  </div>
                </div>
              </div>
             </div>
          </div>
  )
}

export default CustomerForm
