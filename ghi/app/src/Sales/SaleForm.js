import React, {useState, useEffect} from 'react'

function SaleForm() {
    const [salespersons, setSalespersons] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles] = useState([])

    const [salesperson, setSalesperson] = useState('')
    const salespersonchange = (event) => {
        const value = event.target.value;
        setSalesperson(value)
    }

    const [customer, setCustomer] = useState('')
    const customerchange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }

    const [automobile, setAutomobile] = useState('')
    const automobilechange = (event) => {
        const value = event.target.value;
        setAutomobile(value)
    }

    const [price, setPrice] = useState('')
    const pricechange = (event) => {
        const value = event.target.value;
        setPrice(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.sales_person_id = salesperson
        data.customer_id = customer
        data.automobile_id = automobile
        data.price = price

        const salesURL = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(salesURL, fetchConfig)
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale);

            setSalesperson('');
            setCustomer('');
            setAutomobile('');
            setPrice('');
        }


    }


    const salespersonData = async () => {
        const url = 'http://localhost:8090/api/salespersons/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSalespersons(data.sales_persons)
        }
      }
      const customerData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers)
        }
      }
      const automobileData = async () => {
        const url = 'http://localhost:8090/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setAutomobiles(data.automobiles)
        }
      }
        useEffect(() => {
        salespersonData();
        customerData();
        automobileData()
      }, []);
  return (
    <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-sale-form">
                    <h1 className="card-title">Create a sale</h1>

                    <div className="row">
                    <div className="col">
                    <div className="mb-3">
                    <select onChange={salespersonchange} required id="salesperson" name="salesperson" className="form-select" value={salesperson}>
                            <option value="">Choose a sales person</option>
                            {salespersons.map(salespersonss => {
                                return (
                            <option key={salespersonss.id} value={salespersonss.id}>
                            {salespersonss.employee_name}
                            </option>
                    );
                    })}
                    </select>
                    <select onChange={customerchange} required id="customer" name="customer" className="form-select" value={customer}>
                            <option value="">Choose a customer</option>
                            {customers.map(customerss => {
                                return (
                            <option key={customerss.id} value={customerss.id}>
                            {customerss.customer_name}
                            </option>
                    );
                    })}
                    </select>
                    <select onChange={automobilechange} required id="automobile" name="automobile" className="form-select" value={automobile}>
                            <option value="">Choose an automobile</option>
                            {automobiles.map(automobiless => {
                                if ( automobiless.availability == true ) {
                                  return (
                                    <option key={automobiless.id} value={automobiless.id}>
                                    {automobiless.vin}
                                    </option>
                            );
                                }

                    })}
                    </select>
                    <div className="form-floating mb-3">
                        <input onChange={pricechange}  required placeholder="Sale price" type="number" id="price" name="price" className="form-control" value={price}/>
                        <label htmlFor="price">Sale price</label>
                    </div>
              </div>
                        </div>


                      </div>
                      <button className="btn btn-lg btn-primary">Create</button>
                    </form>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                      Congratulations! Your sale has been saved.
                    </div>
                  </div>
                </div>
              </div>
             </div>
          </div>
  )
}

export default SaleForm
