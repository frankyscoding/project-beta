import React, { useState, useEffect } from 'react'

function AutomobileForm() {

    const [models, setModels] = useState([])

    const [color, setColor] = useState('')
    const colorchange = (event) => {
        const value = event.target.value;
        setColor(value)
    }

    const [year, setYear] = useState('')
    const yearchange = (event) => {
        const value = event.target.value;
        setYear(value)
    }

    const [VIN, setVIN] = useState('')
    const vinchange = (event) => {
        const value = event.target.value;
        setVIN(value)
    }

    const [model, setModel] = useState('')
    const modelchange = (event) => {
        const value = event.target.value;
        setModel(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.color = color
        data.year = year
        data.vin = VIN
        data.model_id = model

        const autoURL = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(autoURL, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);

            setColor('');
            setYear('');
            setVIN('');
            setModel('')
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setModels(data.models)


        }
      }

      useEffect(() => {
        fetchData();
      }, []);

  return (
    <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                      <h1 className="card-title">Create a Automobile</h1>

                      <div className="row">
                        <div className="col">
                          <div className="form-floating mb-3">
                            <input onChange={colorchange} value={color} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                          </div>
                          <div className="form-floating mb-3">
                            <input onChange={yearchange} placeholder="Year" required type="text" name ="year" id="year" className="form-control" value={year}/>
                            <label htmlFor="year">Year</label>
                          </div>
                          <div className="form-floating mb-3">
                            <input onChange={vinchange} placeholder="VIN" required type="text" name ="vin" id="vin" className="form-control" value={VIN}/>
                            <label htmlFor="vin">VIN</label>
                          </div>
                          <div className="mb-3">
                            <select onChange={modelchange} required id="model" name="model" className="form-select" value={model}>
                            <option value="">Choose a model</option>
                            {models.map(modelss => {
                                return (
                            <option key={modelss.href} value={modelss.id}>
                            {modelss.name}
                            </option>
                );
                })}
                </select>
              </div>
                        </div>


                      </div>
                      <button className="btn btn-lg btn-primary">Create automobile</button>
                    </form>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                      Congratulations! Your automobile has been saved.
                    </div>
                  </div>
                </div>
              </div>
             </div>
          </div>
  )
}

export default AutomobileForm
