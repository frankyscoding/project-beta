import React, {useEffect, useState} from 'react';

function AppointmentsForm(props) {
    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');
    const [vin, setVin] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [completed, setCompleted] = useState('');


    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.vin = vin;
        data.customer_name = customerName;
        data.date_time = dateTime;
        data.reason = reason;
        data.completed = completed;
        data.technician = technician;

        console.log(data);

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);

            setVin('');
            setCustomerName('');
            setDateTime('');
            setReason('');
            setCompleted('');
            setTechnician('');
        }

    }







    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);


return (
  <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://media.tenor.com/aEbRRiginLwAAAAC/mechanic-hammer.gif" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">New appointments</h1>
                <p className="mb-3">
                  Please schedule a new service appointment here.
                </p>

                <div className="mb-3">
                  <select onChange={handleTechnicianChange} required id="technician" name="technician" className="form-select" >
                    <option value="">Choose a technician to perform this service appointment.</option>
                    {technicians.map((technician) => {
                    return (
                        <option key={technician.id} value={technician.href}>
                            {/* treat the value as it is in insomnia/js/create appointments
                            treat the key as something react can use, since there are multiple
                            conferences with the same exact name in my data base, we should refer to idx to make it unique  */}
                        {technician.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about the rest.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleVinChange} value={vin} required placeholder="vin car" type="text" id="vin" name="vin" className="form-control" />
                      <label htmlFor="vin">vin number</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleCustomerNameChange} value={customerName} required placeholder="customer name" type="customer_name" id="customer_name" name="customer_name" className="form-control" />
                      <label htmlFor="customer_name">Customer name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleDateTimeChange} value={dateTime} required placeholder="date time" type="datetime-local" id="date_time" name="date_time" className="form-control" />
                      <label htmlFor="date_time">Date & Time</label>
                    </div>
                  </div>
                  <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleReasonChange} value={reason} required placeholder="reason" type="text" id="reason" name="reason" className="form-control" />
                    <label htmlFor="reason">reason</label>
                  </div>
                </div>


                </div>
                <button className="btn btn-lg btn-primary">Enter appointment!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! Your pair of appointments has been saved.
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
);

}

export default AppointmentsForm;
