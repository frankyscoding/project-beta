import React, { useEffect, useState } from 'react';



function AppointmentsList( {appointments, fetchAppointments} ) {

    /////////////////////only show upcoming appointments/////////////////////////////////
    function filterAppointments(appointments) {
        const results = []
        for (let appointment of appointments) {
          if (appointment["completed"] != true) {
            results.push(appointment)
          }
        }
        return results;
      }
    console.log(filterAppointments(appointments))
    appointments = filterAppointments(appointments)

    //////////////////////////delete and finish buttons///////////////////////////

    const deleteAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            fetchAppointments()
        }
    }


    const finishAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({ completed: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            fetchAppointments()
        }
    }

    //////////////// ////////////VIP STATUS DETECTION//////////////// ////////////////

    const [automobiles, setAutomobiles] = useState([])


    async function fetchAutomobiles() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        const data = await response.json();
        const automobileData = data.autos
        setAutomobiles(automobileData);
    }

    useEffect(() => {
        fetchAutomobiles();
      }, []);
      console.log(automobiles)


    function vipStatus(appointment, automobiles) {
        let output = []
        for (let object of automobiles) {
          output.push(object["vin"])
        }
        automobiles = output

        if (automobiles.includes(appointment["vin"])) {
            return <img src='https://www.pngfind.com/pngs/m/0-1183_vector-flash-vip-diamond-png-free-photo-clipart.png' width="40" />
        }

        }


    //////////////// //////////////// //////////////// //////////////// ////////////////

    return (
        <>
        <center>
        <h1>Upcoming Appointments</h1>

        </center>

        <table className="table table-striped table-hover align-middle mt-5">

          <thead>
            <tr>
              <th>VIN</th>
              <th>VIP Status?</th>
              <th>Customer name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Cancel?</th>
              <th>Finished?</th>


            </tr>
          </thead>
          <tbody>
            {appointments.map(appointment => {
                return (
                    <tr key={appointment.id}>
                        <td>{ appointment.vin }</td>

                        <td>{ vipStatus(appointment, automobiles) }</td>

                        <td>{ appointment.customer_name }</td>
                        <td>{new Date(appointment.date_time).toLocaleDateString() }</td>
                        <td>{new Date(appointment.date_time).toLocaleTimeString("en-US", {hour: '2-digit', minute:'2-digit'}) }</td>
                        <td>{ appointment.technician }</td>
                        <td>{ appointment.reason }</td>
                        <td>
                            <button className="btn btn-outline-danger" onClick={() => deleteAppointment(appointment)}>Cancel</button>
                        </td>
                        <td>
                            <button className="btn btn-outline-success" onClick={() => finishAppointment(appointment)}>Finished!</button>
                        </td>
                    </tr>
                );
            })}
          </tbody>
        </table>
        </>

    )
}

export default AppointmentsList;
