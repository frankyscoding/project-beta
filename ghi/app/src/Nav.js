import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturers/new">Add New Manufacturer</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturers">Manufacturers List</NavLink>
              </li>

               <li className="nav-item">
                <NavLink className="nav-link" to="/models/new">Add New Vehicle Model</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/models">Vehicle Models List</NavLink>
              </li>

               <li className="nav-item">
                <NavLink className="nav-link" to="/automobiles">Automobiles List</NavLink>
              </li>

               <li className="nav-item">
                <NavLink className="nav-link" to="/automobiles/new">Add New Automobile</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/technicians/new">Enter a technician</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/appointments/new">Enter a service appointment</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/appointments">View appointments</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/appointments/vin">Service History</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/salesperson/new">Add sales employee</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/customer/new">Add customer</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/sales">View all sales</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/sales/new">Add sale</NavLink>
              </li>

              <li className="nav-item">
                <NavLink className="nav-link" to="/saleshistory">View Sales History</NavLink>
              </li>







          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
