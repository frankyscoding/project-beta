import React from 'react';
import { Link } from "react-router-dom";



function VehicleModelsList({ models }) {
    if (models === undefined) {
        return null
    }




    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Vehicle Model Name</th>
              <th>Manufacturer name</th>
              <th>Picture</th>


            </tr>
          </thead>
          <tbody>
            {models.map(model => {
                return (
                    <tr key={model.href}>
                        <td>{ model.name }</td>
                        <td>{ model.manufacturer.name }</td>
                        <td><img src={ model.picture_url } className="img-thumbnail shoes" width="300"></img></td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default VehicleModelsList;
