from django.urls import path

from .views import (
    api_list_appointments,
    api_show_appointment,
    api_list_technicians,
    # api_list_vin_history,
)

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_create_appointment"),
    path("technicians/", api_list_technicians, name="api_create_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    # path(
    #     "appointments/<int:vin_vo_id>/",
    #     api_list_vin_history,
    #     name="api_list_vin_history",
    # ),
    #will list appointments for that VIN^

]
