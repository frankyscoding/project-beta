from django.shortcuts import render
import json
from django.http import JsonResponse
from .models import Appointment, Technician, VinVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class VinVODetailEncoder(ModelEncoder):
    model = VinVO
    properties = [
        "vin"
    ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
        ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date_time",
        "reason",
        "completed",
        "technician",
        ]

    def get_extra_data(self, o):
        return {"technician": o.technician.name}




class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date_time",
        "reason",
        "completed",
        "technician",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_appointments(request, technician_vo_id=None):
    if request.method == "GET":
        if technician_vo_id is not None:
            appointments = Appointment.objects.filter(technician=technician_vo_id)
        else:
            appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            name = content["technician"]
            technician = Technician.objects.get(name=name)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician name"},
                status=400,
            )
        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "PUT","DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
        )
        except Appointment.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid appointment id"},
            status=400,
            )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                id = content["technician"]
                technician = Technician.objects.get(id=id)
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,)
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"appointment has been deleted": count > 0})





@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )





def api_list_vin_history(request, vin_vo_id=None):
    if request.method == "GET":
        if vin_vo_id is not None:
            appointments = Appointment.objects.filter(vin=vin_vo_id)
        else:
            appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
        )


# @require_http_methods(["GET"])
# def api_list_vin_history(request, pk):
#     if request.method == "GET":
#         try:
#             appointments = Appointment.objects.filter(vin=pk)
#             return JsonResponse(
#             {"VIN service history": appointments},
#             encoder=AppointmentListEncoder,
#         )
#         except Appointment.DoesNotExist:
#             return JsonResponse(
#             {"message": "Invalid appointment id"},
#             status=400,
#             )
