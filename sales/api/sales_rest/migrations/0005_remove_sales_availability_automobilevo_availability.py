# Generated by Django 4.0.3 on 2023-01-25 22:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0004_rename_availablity_sales_availability'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sales',
            name='availability',
        ),
        migrations.AddField(
            model_name='automobilevo',
            name='availability',
            field=models.BooleanField(default=True),
        ),
    ]
