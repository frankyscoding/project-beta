from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import SalesPerson, Customer, AutomobileVO, Sales
from common.json import ModelEncoder

class SalesPersonsEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["employee_name", "employee_number", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["customer_name", "address", "phone_number", "id"]

class AutomobileEncoderVO(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "color", "year", "vin", "import_href", "availability"]

class SalesEncoder(ModelEncoder):
    model = Sales
    properties = ["id", "sales_person", "customer", "automobile", "price",]

    encoders = {
        "sales_person": SalesPersonsEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileEncoderVO(),
    }


@require_http_methods(["GET", "POST"])
def sales_persons(request):
    if request.method == "GET":
        persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": persons},
            encoder = SalesPersonsEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sales_persons = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_persons,
                encoder=SalesPersonsEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the manufacturer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPersonsEncoder.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonsEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonsEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            potential_customers = Customer.objects.create(**content)
            return JsonResponse(
                potential_customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the manufacturer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def customer(request, pk):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["GET"])
def automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder = AutomobileEncoderVO
        )

@require_http_methods(["PUT"])
def automobile(request, vin):
    if request.method == "PUT":
        automobile = AutomobileVO.objects.filter(vin=vin).update(availability=False)
        automobile.save()
        return JsonResponse(
            automobile,
            encoder = AutomobileEncoderVO,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def sales(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder = SalesEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            #automobile = AutomobileVO.objects.get(vin=content["automobile"])
            #if automobile.availability == True:
            salesperson_id = content["sales_person_id"]
            sales_person = SalesPerson.objects.get(id=salesperson_id)
            content["sales_person"] = sales_person
            customer_id = content["customer_id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            automobile_id = content["automobile_id"]
            automobile = AutomobileVO.objects.get(id=automobile_id)
            content["automobile"] = automobile
            if automobile.availability is True:
                automobile.availability = False
                automobile.save()

            #automobile.availability = False
            #automobile.save()



            model = Sales.objects.create(**content)
            return JsonResponse(
                model,
                encoder=SalesEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def sale(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sales.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
